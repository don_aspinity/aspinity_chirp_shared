/*
 * led_test.c
 * 
 * Copyright 2017  <pi@raspberrypi>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <wiringPi.h>
#include <softPwm.h>
#include <stdio.h>
#include <stdlib.h>

// using wiringPi pin numbers
#define LED_PIN_1 30
#define LED_PIN_2 21
#define NR_PINS 2

// in milliseconds
#define ACTIVITY_TIME 5000

void initializeLEDs(int pin[], int nrPins);
void phaseOn(int pin[], int nrPins, int time);
void phaseOff(int pin[], int nrPins, int time);
void blink(int pin[], int nrPins, int repeat, int time);
void writeLED(int pin[], int nrPins, int level);

// pins
int pins[NR_PINS];

// timing
int endActivity;
unsigned int currentTime;
unsigned int startTime;

PI_THREAD (timingThread){
	while (1){
		currentTime = millis();
		if (currentTime - startTime >= ACTIVITY_TIME){
			writeLED(pins, NR_PINS, LOW);
			exit(0);
		}
	}
}

int main(int argc, char **argv)
{	
	// pins
	pins[0] = LED_PIN_1;
	pins[1] = LED_PIN_2;
	
	// initialize
	wiringPiSetup();
	initializeLEDs(pins, NR_PINS);
	
	// pi_thread for timing
	startTime = millis();
	piThreadCreate(timingThread);
	
	// do activity
	while (1){
		phaseOn(pins, NR_PINS, 20);
		phaseOff(pins, NR_PINS, 20);		
		blink(pins, NR_PINS, 3, 100);
	}
	
	return 0;
}

void initializeLEDs(int pin[], int nrPins){
	int i;
	for(i = 0; i < nrPins; i++){
		softPwmCreate(pin[i], 0, 100);
	}
}

void phaseOn(int pin[], int nrPins, int time){
	int i;
	for(i = 0; i <= 100; i++){
		int n;
		for (n = 0; n < nrPins; n++){
			softPwmWrite(pin[n], i);
		}	
		delay(time);
	}
}

void phaseOff(int pin[], int nrPins, int time){
	int i;
	for(i = 100; i >= 0; i--){
		int n;
		for (n = 0; n < nrPins; n++){
			softPwmWrite(pin[n], i);
		}		
		delay(time);
	}
}

void blink(int pin[], int nrPins, int repeat, int time){
	int i;
	for (i = 0; i < repeat; i++){
		writeLED(pin, nrPins, HIGH);
		delay(time);
		writeLED(pin, nrPins, LOW);
		delay(time);		
	} 
}

void writeLED(int pin[], int nrPins, int level){
	int i;
	for (i = 0; i < nrPins; i++){
		digitalWrite(pin[i], level);
	}
}

